-- a ja ref a on taulut mihin halutaan laittaa juttuja
create table a (
       id serial primary key,
       t text
);

create table ref_a (
       refid serial primary key,
       refval text,
       avain integer references a
);


-- b ja ref_b on tauluja
create table b (
       id serial primary key,
       t text
);

create table ref_b (
       refid serial primary key,
       refval text,
       avain integer references b
);


insert into a (t) values ('a1'), ('a2'), ('a3');
insert into ref_a (refval, avain) values ('ref_a1', 1), ('ref_a2', 2), ('ref_a3', 3);

-- b:llä ja ref_b:llä on päällekäisiä avaimia
insert into b (t) values ('b1'), ('b2'), ('b3');
insert into ref_b (refval, avain) values ('ref_b1', 1), ('ref_b2', 2), ('ref_b3', 3);



-- tämä transaktio siirtää b:n jutut a:han muuttaen fiksusti b:n avaimet ja korjaten a:n sekvenssit
begin transaction;

-- ref_b:n olisi voinut luoda alunperin on update cascadella mutta jos sitä ei ole, näin sen voi laittaa
alter table ref_b
      drop constraint ref_b_avain_fkey,
      add constraint ref_b_avain_fkey foreign key (avain) references b (id) on update cascade;

-- kasvatetaan b:n ja ref_b:n id-kenttien sekvenssejä niin, että ne
-- eivät ole päällekäin a:n ja ref_a:n sekvenssien kanssa
-- currval('a_id_seq') antaa a_id_seq-sekvenssin nykyisen arvon
-- sekvenssien nimet saat esille psql:ssä \d-komennolla
update b set id = id + currval('a_id_seq');
update ref_b set refid = refid + currval('ref_a_refid_seq');

-- siirretään b:n ja ref_b:n arvot
insert into a (id, t) (select id, t from b);
insert into ref_a (refid, refval, avain) (select refid, refval, avain from ref_b);

-- korjataan a:n ja ref_a:n id-sekvenssit oikeaan arvoon
select setval('a_id_seq', currval('a_id_seq') + currval('b_id_seq'));
select setval('ref_a_refid_seq', currval('ref_a_refid_seq') + currval('ref_b_refid_seq'));

end transaction;
